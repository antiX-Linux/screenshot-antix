��          �      L      �     �     �     �     �  .     	   1     ;     G     ]     r     �     �     �     �     �     �     �              $     9     N     ^  >   p     �     �     �     �     �          -     I     c  &   �     �  $   �     �                                	                                                     
               Choose directory Create a thumbnail Custom Area Delay in seconds File extension was changed.\nPlease try again. File type Full Screen Keep this screenshot? Multiple screenshots Name of image Percentage (1-100) Region to capture Show save dialog Take another shot Thumbnail for webpages Window Your screenshot has been saved. antiXscreenshot Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-15 17:00+0200
Last-Translator: Moo
Language-Team: Lithuanian (http://www.transifex.com/anticapitalista/antix-development/language/lt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: lt
Plural-Forms: nplurals=4; plural=(n % 10 == 1 && (n % 100 > 19 || n % 100 < 11) ? 0 : (n % 10 >= 2 && n % 10 <=9) && (n % 100 > 19 || n % 100 < 11) ? 1 : n % 1 != 0 ? 2: 3);
X-Generator: Poedit 2.2.1
 Pasirinkti katalogą Sukurti miniatiūrą Tinkinta sritis Delsa sekundėmis Failo prievardis buvo pakeistas.\nPrašome bandyti dar kartą. Failo tipas Visas ekranas Palikti šią ekrano kopiją? Kelios ekrano kopijos Paveikslo pavadinimas Procentinė dalis (1-100) Sritis, kurią fotografuoti Rodyti įrašymo dialogą Padaryti kitą ekrano kopiją Miniatiūra internetinėms svetainėms Langas Jūsų ekrano kopija buvo įrašyta. antiXscreenshot 