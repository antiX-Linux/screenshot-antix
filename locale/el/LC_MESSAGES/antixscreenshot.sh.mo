��          �      L      �     �     �     �     �  .     	   1     ;     G     ]     r     �     �     �     �     �     �     �       �    (   �  6   �  )   %  4   O  K   �     �     �  0     !   3  %   U     {  +   �  6   �     �  9        M  2   ^     �                                	                                                     
               Choose directory Create a thumbnail Custom Area Delay in seconds File extension was changed.\nPlease try again. File type Full Screen Keep this screenshot? Multiple screenshots Name of image Percentage (1-100) Region to capture Show save dialog Take another shot Thumbnail for webpages Window Your screenshot has been saved. antiXscreenshot Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-15 17:00+0200
Last-Translator: anticapitalista <anticapitalista@riseup.net>
Language-Team: Greek (http://www.transifex.com/anticapitalista/antix-development/language/el/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: el
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.2.1
 Επιλέξτε τον κατάλογο Δημιουργήστε μια μικρογραφία Προσαρμοσμένη περιοχή Καθυστέρηση σε δευτερόλεπτα Επέκταση αρχείου άλλαξε.\nΔοκιμάστε ξανά. Τύπος αρχείου Πλήρης Οθόνη Κρατήστε αυτήν την εικόνα; Πολλαπλές εικόνες Το όνομα της εικόνας Ποσοστό (1-100) Περιοχή για να συλλάβει Εμφάνιση Αποθήκευση διαλόγου Πάρτε έναν άλλο  Γραφική σύνοψη για ιστοσελίδες Παράθυρο Screenshot σας έχει αποθηκευτεί. antiXscreenshot 