��          �      L      �     �     �     �     �  .     	   1     ;     G     ]     r     �     �     �     �     �     �     �       �       �     �     �     �  8        ;     I     Y     p     �     �     �     �     �     �               *                                	                                                     
               Choose directory Create a thumbnail Custom Area Delay in seconds File extension was changed.\nPlease try again. File type Full Screen Keep this screenshot? Multiple screenshots Name of image Percentage (1-100) Region to capture Show save dialog Take another shot Thumbnail for webpages Window Your screenshot has been saved. antiXscreenshot Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-15 17:00+0200
Last-Translator: Hannes_Worst <hannesworst@gmail.com>
Language-Team: Dutch (http://www.transifex.com/anticapitalista/antix-development/language/nl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nl
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.2.1
 Kies een map Thumbnail aanmaken Aangepast gebied Uitstel in seconden Bestandsextensie is gewijzigd.\nA.u.b. opnieuw proberen. Bestandssoort Volledig scherm Schermafdruk behouden? Meerdere schermafdrukken Naam van de afbeelding Percentage (1-100) Afdrukgebied Toon bewaar-dialoog Nog een afdruk maken Thumbnail voor webpagina's Scherm Uw schermafdruk is opgeslagen. antiXschermafdruk 