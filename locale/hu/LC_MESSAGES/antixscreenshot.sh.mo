��          �      L      �     �     �     �     �  .     	   1     ;     G     ]     r     �     �     �     �     �     �     �       �       �     �     �       7   +     c     p     �     �     �     �     �     �                >     D     d                                	                                                     
               Choose directory Create a thumbnail Custom Area Delay in seconds File extension was changed.\nPlease try again. File type Full Screen Keep this screenshot? Multiple screenshots Name of image Percentage (1-100) Region to capture Show save dialog Take another shot Thumbnail for webpages Window Your screenshot has been saved. antiXscreenshot Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-15 16:58+0200
Last-Translator: Tamás Szekeres <szekeres.tamas@icloud.com>
Language-Team: Hungarian (http://www.transifex.com/anticapitalista/antix-development/language/hu/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hu
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.2.1
 Könyvtár választása Előnézeti kép létrehozása Egyedi terület Késleltetés másodpercben A fájl kiterjesztése megváltozott.\nPróbáld újra. Fájl típus Teljes képernyő Megtartod ezt a képet? Többszörös képernyőmentés A kép neve Százalék (1-100) Terület kijelölése Mutasd a mentő ablakot Másik kép készítése Előnézeti kép WEBoldalakoz Ablak Képernyő kép el lett mentve. antiXKépernyőkép készítő 