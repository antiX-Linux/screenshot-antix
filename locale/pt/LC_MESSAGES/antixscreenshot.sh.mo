��          �      L      �     �     �     �     �  .     	   1     ;     G     ]     r     �     �     �     �     �     �     �       �       �     �     �     �  8   
     C     T     b     �     �     �     �     �     �     �                :                                	                                                     
               Choose directory Create a thumbnail Custom Area Delay in seconds File extension was changed.\nPlease try again. File type Full Screen Keep this screenshot? Multiple screenshots Name of image Percentage (1-100) Region to capture Show save dialog Take another shot Thumbnail for webpages Window Your screenshot has been saved. antiXscreenshot Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-15 17:01+0200
Last-Translator: José Vieira <jvieira33@sapo.pt>
Language-Team: Portuguese (http://www.transifex.com/anticapitalista/antix-development/language/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.2.1
 Escolher um diretório Criar uma miniatura Personalizada Atraso em segundos A extensão de ficheiro foi alterada.\nTentar novamente. Tipo de ficheiro Ecrã inteiro Manter esta captura de ecrã? Capturas múltiplas Nome da imagem Percentagem (1-100) Área de captura Mostrar diálogo de gravação Efetuar outra captura Miniatura para a web Janela A captura de ecrã foi guardada. antiX-Captura de ecrã 