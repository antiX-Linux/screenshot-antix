��          �      L      �     �     �     �     �  .     	   1     ;     G     ]     r     �     �     �     �     �     �     �       4       K     [     n     }  1   �  	   �     �     �     �               #     >     [     s     �  $   �     �                                	                                                     
               Choose directory Create a thumbnail Custom Area Delay in seconds File extension was changed.\nPlease try again. File type Full Screen Keep this screenshot? Multiple screenshots Name of image Percentage (1-100) Region to capture Show save dialog Take another shot Thumbnail for webpages Window Your screenshot has been saved. antiXscreenshot Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-15 17:01+0200
Last-Translator: Filip Bog <mxlinuxpl@gmail.com>
Language-Team: Polish (http://www.transifex.com/anticapitalista/antix-development/language/pl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pl
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);
X-Generator: Poedit 2.2.1
 Wybierz katalog Stwórz miniaturę Własny obszar Opóźnienie w sekundach Zmieniono rozszerzenie pliku.\nSpróbuj ponownie. Typ pliku Pełny ekran Zachować zrzut? Wiele zrzutów ekranu Nazwa obrazu Procent (1-100) Obszar do wykonania zrzutu Pokaż okno dialogowe zapisu Wykonaj następny zrzut Miniatura na stronę www Okno Twój zrzut ekranu został zapisany. antiX zrzut ekranu 