��          �      L      �     �     �     �     �  .     	   1     ;     G     ]     r     �     �     �     �     �     �     �       �            %     9     I  ;   d     �     �     �     �     �            "   *     M  #   g     �  $   �     �                                	                                                     
               Choose directory Create a thumbnail Custom Area Delay in seconds File extension was changed.\nPlease try again. File type Full Screen Keep this screenshot? Multiple screenshots Name of image Percentage (1-100) Region to capture Show save dialog Take another shot Thumbnail for webpages Window Your screenshot has been saved. antiXscreenshot Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-15 16:59+0200
Last-Translator: anticapitalista <anticapitalista@riseup.net>
Language-Team: Czech (http://www.transifex.com/anticapitalista/antix-development/language/cs/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: cs
Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n <= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;
X-Generator: Poedit 2.2.1
 Vyberte složku Vytvořit miniaturu Vlastní oblast Zpoždění ve vteřinách Přípona souboru byla změněna.\nZkuste to prosím znovu. Typ souboru Celá obrazovka Uchovat tento snímek obrazovky Mnoho snímků obrazovky Název obrázku Procento (1-100) Oblast k zachycení Zobrazit dialogové okno uložení Pořídit další snímek Miniatury pro internetové stránky Okno Váš snímek obrazovky byl uložen. antiXscreenshot 