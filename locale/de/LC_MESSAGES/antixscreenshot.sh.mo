��          �      L      �     �     �     �     �  .     	   1     ;     G     ]     r     �     �     �     �     �     �     �       �       �     �     �     �  A        Y     b  *   k     �     �     �     �     �               ;  %   C     i                                	                                                     
               Choose directory Create a thumbnail Custom Area Delay in seconds File extension was changed.\nPlease try again. File type Full Screen Keep this screenshot? Multiple screenshots Name of image Percentage (1-100) Region to capture Show save dialog Take another shot Thumbnail for webpages Window Your screenshot has been saved. antiXscreenshot Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-15 17:00+0200
Last-Translator: Martin O'Connor <mar.oco@arcor.de>
Language-Team: German (http://www.transifex.com/anticapitalista/antix-development/language/de/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.2.1
 Ordner wählen Vorschaubild erstellen benutzerdefinierter Bereich Verzögerung in Sekunden Dateierweiterung wurde geändert.\nBitte versuchen Sie es erneut. Dateityp Vollbild Wollen Sie dieses Bildschirmfoto behalten? Mehrere Bildschirmfotos Name des Bildes Prozentsatz (1-100) zu erfassender Bereich Zeige den Speichern-Dialog Nimm eine weitere Aufnahme Vorschaubild für Webseiten Fenster Ihr Bildschirmfoto wurde gespeichert. antiXscreenshot 