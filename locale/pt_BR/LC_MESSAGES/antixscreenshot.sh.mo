��          �      L      �     �     �     �     �  .     	   1     ;     G     ]     r     �     �     �     �     �     �     �       �       �     �     �       D        `     p     }     �     �     �     �     �     	  #        B      I  *   j                                	                                                     
               Choose directory Create a thumbnail Custom Area Delay in seconds File extension was changed.\nPlease try again. File type Full Screen Keep this screenshot? Multiple screenshots Name of image Percentage (1-100) Region to capture Show save dialog Take another shot Thumbnail for webpages Window Your screenshot has been saved. antiXscreenshot Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-11-27 21:20+0200
Last-Translator: marcelo cripe <marcelocripe@gmail.com>
Language-Team: Portuguese (Brazil) (http://www.transifex.com/anticapitalista/antix-development/language/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 2.3
 Escolher o diretório Criar uma miniatura Personalizada Atraso em segundos A extensão do arquivo foi modificada. \nPor favor, tente novamente. Tipo de arquivo Tela inteira Manter esta captura de tela? Múltiplas capturas de tela Nome da imagem Porcentagem (1-100) Área de captura Mostrar diálogo de gravação Continuar a capturar Miniatura para páginas de internet Janela A sua captura de tela foi salva. antiXscreenshot - Captura de Tela do antiX 