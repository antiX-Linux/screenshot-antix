��          �      L      �     �     �     �     �  .     	   1     ;     G     ]     r     �     �     �     �     �     �     �       �       �     �     �       @        [     k     x     �     �     �     �      �               6  +   ?     k                                	                                                     
               Choose directory Create a thumbnail Custom Area Delay in seconds File extension was changed.\nPlease try again. File type Full Screen Keep this screenshot? Multiple screenshots Name of image Percentage (1-100) Region to capture Show save dialog Take another shot Thumbnail for webpages Window Your screenshot has been saved. antiXscreenshot Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-15 17:00+0200
Last-Translator: cyril cottet <cyrilusber2001@yahoo.fr>
Language-Team: French (http://www.transifex.com/anticapitalista/antix-development/language/fr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 2.2.1
 Donner le choix du répertoire Créer une miniature Zone personnalisée Délai en secondes L'extension de fichier a été modifiée. \nVeuillez réessayer. Type de fichier Plein écran Garder cette capture d'écran?  Captures multiples Nom de l'image Pourcentage (1-100) Région à capturer Boîte de dialogue 'enregistrer' Prendre une autre capture Miniature pour pages web Fenêtre Votre capture d'écran a été sauvegardée Capture d'écran pour antiX 