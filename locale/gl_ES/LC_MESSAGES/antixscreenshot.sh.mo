��          �      L      �     �     �     �     �  .     	   1     ;     G     ]     r     �     �     �     �     �     �     �       �       �     �            9   "     \     m           �     �     �     �     �          #     8  "   ?     b                                	                                                     
               Choose directory Create a thumbnail Custom Area Delay in seconds File extension was changed.\nPlease try again. File type Full Screen Keep this screenshot? Multiple screenshots Name of image Percentage (1-100) Region to capture Show save dialog Take another shot Thumbnail for webpages Window Your screenshot has been saved. antiXscreenshot Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-11-27 21:20+0200
Last-Translator: David Rebolo Magariños <drgaga345@gmail.com>
Language-Team: Galician (Spain) (http://www.transifex.com/anticapitalista/antix-development/language/gl_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: gl_ES
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Escoller un directorio Crear unha miniatura Personalizada Atraso en segundos A extensión do ficheiro foi alterada.\nIntentar de novo. Tipo de ficheiro Pantalla completa Gardar esta captura de pantalla? Múltiples capturas de pantalla Nome da imaxe Porcentaxe (1-100) Área de captura Amosar o diálogo gardado Efectuar outra captura Miniatura para a web Lapela A captura de pantalla foi gardada. antiX-Captura de pantalla 