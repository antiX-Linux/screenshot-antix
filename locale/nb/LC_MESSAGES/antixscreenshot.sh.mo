��          �      L      �     �     �     �     �  .     	   1     ;     G     ]     r     �     �     �     �     �     �     �       �    
   �     �     �        )        A     I     W     k     ~     �     �     �     �     �     �     �                                     	                                                     
               Choose directory Create a thumbnail Custom Area Delay in seconds File extension was changed.\nPlease try again. File type Full Screen Keep this screenshot? Multiple screenshots Name of image Percentage (1-100) Region to capture Show save dialog Take another shot Thumbnail for webpages Window Your screenshot has been saved. antiXscreenshot Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-06-16 15:11+0300
Last-Translator: Kjell Cato Heskjestad <cato@heskjestad.xyz>
Language-Team: Norwegian Bokmål (http://www.transifex.com/anticapitalista/antix-development/language/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Velg mappe Lag et miniatyrbilde Selvvalgt område Forsinkelse i sekunder Filetternavnet ble endret.\Forsøk igjen. Filtype Hele skjermen Behold skjermbilde? Flere skjermbilder Bildets navn Prosent (1-100) Område Vis lagringsdialog Ta et nytt skjermbilde Miniatyrbilde av nettsider Vindu Skjermbildet ble lagret. antiXscreenshot 