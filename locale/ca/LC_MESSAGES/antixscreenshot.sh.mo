��          �      L      �     �     �     �     �  .     	   1     ;     G     ]     r     �     �     �     �     �     �     �       �       �     �     �     �  @   �     ;     K     ]     s     �     �     �     �     �     �     �  #        (                                	                                                     
               Choose directory Create a thumbnail Custom Area Delay in seconds File extension was changed.\nPlease try again. File type Full Screen Keep this screenshot? Multiple screenshots Name of image Percentage (1-100) Region to capture Show save dialog Take another shot Thumbnail for webpages Window Your screenshot has been saved. antiXscreenshot Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-15 16:59+0200
Last-Translator: Eduard Selma <selma@tinet.cat>
Language-Team: Catalan (http://www.transifex.com/anticapitalista/antix-development/language/ca/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ca
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.2.1
 Trieu directori Crea una miniatura Àrea a mida Retard en segons Extensió del fitxer incorrecta.
Si us plau, torneu-ho a provar. Tipus de fitxer Pantalla completa Desa aquesta captura? Captures múltiples Nom de la imatge Percentatge (1-100) Regió a capturar Diàleg per desar Feu una altra captura Miniatures per a web Finestra La captura s'ha desat correctament. antiXscreenshot 