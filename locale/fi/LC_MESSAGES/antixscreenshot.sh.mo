��          �      L      �     �     �     �     �  .     	   1     ;     G     ]     r     �     �     �     �     �     �     �       �       �     �     �     �  6        D     R     `       
   �     �     �     �     �     �                5                                	                                                     
               Choose directory Create a thumbnail Custom Area Delay in seconds File extension was changed.\nPlease try again. File type Full Screen Keep this screenshot? Multiple screenshots Name of image Percentage (1-100) Region to capture Show save dialog Take another shot Thumbnail for webpages Window Your screenshot has been saved. antiXscreenshot Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-15 17:00+0200
Last-Translator: En Kerro <inactive+ekeimaja@transifex.com>
Language-Team: Finnish (http://www.transifex.com/anticapitalista/antix-development/language/fi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fi
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.2.1
 Valitse hakemisto Luo esikatselukuva Määritelty alue Viive sekunteina Tiedostomuoto muuttui.\nOle hyvä ja yritä uudelleen. Tiedostomuoto Koko näyttö Säilytä tämä kuvakaappaus? Useita kuvakaappauksia Kuvan nimi Prosenttia (1-100) Kaapattava alue Näytä tallennusvalikko Ota toinen kaappaus Esikatselukuva nettisivulle Ikkuna Kuvakaappauksesi on tallennettu. antiXkuvakaappaus 